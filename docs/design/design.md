# Tape Transfer Manager Design

## Purpose

The Tape Transfer Manager (TTM) is intended to provide a light-weight solution to the task of data management for large scale sneakerware transfers between remote sites. The principal flow of transfers is illustrated below: 

![](system_flow.png)

Data available at the site of Tape Archive 1 may be acquired and streamed to the archive upon request to the TTM API. Tapes are then automatically flagged for physical transfer. After transfer, the tapes are unloaded with project dependent handling services managing the onward migration of data to their final sites. 

## System components

### Storage Proxies

Storage proxies provide a means for the tape loader to acquire data from multiple sources through different mechansisms (e.g. object store, file system, rsync transfer). Transfer requests made to the API must specify a storage proxy through which data can be read and an identifier (nominally a filename) to be passed to the proxy `fetch` method. 

A second function of storage proxies is to provide fast (potentially batched) access to data for transfer. This is required as to ensure efficient tape writing. Proxies may provide this capability through staging of data on fast storage local to the archive interface. 

Storage proxies must implement an asynchronous `fetch` and `store` method. They should be non-blocking until they reach some pre-defined maximum number concurrent fetch or store operations.

### Transfer System API

The Transfer System API provides a ReSTful interface through which  users can request transfers between sites. The API also provides limited possibility for editing the properties of transfer requests. The API also exposes monitoring information for the system. 

### Tape Loader

The Tape Loader queries the API to find data to transfer and tapes which can be written to. It then uses the storage proxies to acquire the data and migrate it to tape.

### Tape Unloader

The Tape Unloader identifies tapes to be unloaded and passes the data on to project-based transfer services for further handling. The tape unloader requires confirmation from the tansfer services on the successful migration of data to allow for transfer requests to be marked completed.

### Transfer Service

Transfer Services handle data unloaded from tape. This may be as simple as migrating a file to a local filesystem or it may be as complex as modifying the file and transferring it via network to another remote facility. As with data acquisition, data storage here is delegated to storage proxies specific to each of the end destinations for the data.

### Transfer System DB

The transfer system DB is a relational database that provides the data model for the TTM. 

![](data_model.png)

### Questions

- What happens if I write a file to tape that does not have sufficient capacity. Do I need to rewind to last EOF marker and erase?
- What kind of error is thrown?
- Is the tape useage deterministic when not using compression?
- How does block size affect usage?





