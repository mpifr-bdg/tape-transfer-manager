import unittest
from unittest import mock
import logging
import os
import tempfile
from tape_transfer_manager import storage_proxies as sp


def find_all(name, path):
    result = []
    for root, dirs, files in os.walk(path):
        if name in files:
            result.append(os.path.join(root, name))
    return result


class Test(unittest.IsolatedAsyncioTestCase):

    def setUp(self):
        self.staging_dir = tempfile.TemporaryDirectory()

    def tearDown(self):
        self.staging_dir.cleanup()

    def make_data_descriptor(self, base_path, nfiles):
        temp_files = []
        data_products = []
        for ii in range(nfiles):
            tfile = tempfile.NamedTemporaryFile()
            temp_files.append(tfile)
            data_products.append({
                "source_path": tfile.name,
                "transfer_filename": f"test_file_{ii}"
                })
        data_descriptor = {
            "base_transfer_path": base_path,
            "data_products": data_products
        }
        return data_descriptor, temp_files

    def make_mock_request(self, tr_id, data_descriptor):
        transfer_request = mock.Mock()
        transfer_request.__repr__ = mock.MagicMock(
            return_value=f"TestTransferRequest-{tr_id}")
        transfer_request.get_data_descriptor = mock.MagicMock(
            return_value=data_descriptor)
        transfer_request.id = tr_id
        return transfer_request

    def check_files(self, data_descriptor, transfer_request, exists=True):
        for data_product in data_descriptor["data_products"]:
            fname = data_product["transfer_filename"]
            base_path = os.path.join(
                self.staging_dir.name, str(transfer_request.id))
            hits = find_all(fname, base_path)
            if exists:
                self.assertTrue(len(hits) == 1)
            else:
                self.assertTrue(len(hits) == 0)

    async def test_stage_unstage(self):
        """
        Test a simple staging followed by unstaging
        """
        dd, temp_files = self.make_data_descriptor(
            "/projects/test/", 5)
        transfer_request = self.make_mock_request(1, dd)
        proxy = sp.LocalFileSystemProxy(self.staging_dir.name)

        await proxy.stage(transfer_request)
        # Check that each of the expected file names
        # is located under the correct path
        self.check_files(dd, transfer_request)
        await proxy.unstage(transfer_request)

        # check that the original files have not been deleted
        # by the unstaging:
        for tfile in temp_files:
            self.assertTrue(os.path.isfile(tfile.name))

    async def test_unstage_without_prior_stage(self):
        """
        Check that unstaging a request without staging does not
        raise an exception
        """
        dd, temp_files = self.make_data_descriptor(
            "/projects/test/", 5)
        transfer_request = self.make_mock_request(1, dd)
        proxy = sp.LocalFileSystemProxy(self.staging_dir.name)
        await proxy.unstage(transfer_request)
        # check that the original files have not been deleted
        # by the unstaging:
        for tfile in temp_files:
            self.assertTrue(os.path.isfile(tfile.name))

    async def test_stage_unstage_multiple(self):
        """
        Check that multiple requests can be staged simultaneously
        """
        dd_1, temp_files_1 = self.make_data_descriptor(
            "/projects/test/", 3)
        dd_2, temp_files_2 = self.make_data_descriptor(
            "/projects/toast/", 3)
        transfer_request_1 = self.make_mock_request(1, dd_1)
        transfer_request_2 = self.make_mock_request(2, dd_2)
        proxy = sp.LocalFileSystemProxy(self.staging_dir.name)

        await proxy.stage(transfer_request_1)
        await proxy.stage(transfer_request_2)
        await proxy.unstage(transfer_request_1)

        self.check_files(dd_2, transfer_request_2, exists=True)
        self.check_files(dd_1, transfer_request_1, exists=False)
        # check that the original files have not been deleted
        # by the unstaging:
        for tfile in temp_files_1:
            self.assertTrue(os.path.isfile(tfile.name))
        for tfile in temp_files_2:
            self.assertTrue(os.path.isfile(tfile.name))

    def test_proxy_lookup(self):
        """
        Test that reflection based lookups work
        on the storage proxy name
        """
        lookup = sp.STORAGE_PROXIES.get("LocalFileSystem", None)
        self.assertTrue(lookup == sp.LocalFileSystemProxy)

    async def test_malformed_descriptor(self):
        """
        Test staging with a data descriptor with missing keys.
        These should raise KeyErrors
        """
        proxy = sp.LocalFileSystemProxy(self.staging_dir.name)
        dd, temp_files = self.make_data_descriptor(
            "/projects/test/", 0)
        del dd["data_products"]
        transfer_request = self.make_mock_request(1, dd)
        with self.assertRaises(KeyError):
            await proxy.stage(transfer_request)

    async def test_missing_source_file(self):
        """
        Test that staging fails if the specified
        source is not found.
        """
        proxy = sp.LocalFileSystemProxy(self.staging_dir.name)
        dd, temp_files = self.make_data_descriptor(
            "/projects/test/", 1)
        dd["data_products"][0]["source_path"] = "not_a_file"
        transfer_request = self.make_mock_request(1, dd)
        with self.assertRaises(FileNotFoundError):
            await proxy.stage(transfer_request)


if __name__ == "__main__":
    logger = logging.getLogger("storage_proxies")
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        ("[ %(levelname)s - %(asctime)s - %(name)s - "
         "%(filename)s:%(lineno)s] %(message)s"))
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    unittest.main()
