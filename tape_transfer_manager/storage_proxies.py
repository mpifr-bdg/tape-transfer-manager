import os
import shutil
import logging

logger = logging.getLogger("storage_proxies")

STORAGE_PROXIES = {}


def register_storage_proxy(name):
    def wrapper(cls):
        if name in STORAGE_PROXIES:
            raise Exception(f"Storage proxy '{name}' already registered")
        else:
            STORAGE_PROXIES[name] = cls
        return cls
    return wrapper


@register_storage_proxy("LocalFileSystem")
class LocalFileSystemProxy:
    """
    A storage proxy for accessing local files.

    Operates by creating a directory structure of symlinks
    that can be written to the archiving medium with a tar
    call.

    As the tar call is operating on symlinks, it is necessary
    to specify the '--dereference' argument when creating
    the tar ball.
    """
    def __init__(self, staging_path):
        """
        Crete a new instance

        :param      staging_path:  The base directory where transfer
                                   requests should be staged.
        :type       staging_path:  String

        :note   The staging path will be created if it does not
                already exist.
        """

        self._staging_path = staging_path
        os.makedirs(self._staging_path, exist_ok=True)

    def verify_file(self, path):
        # as linux symbolic linking doesn't require the linked
        # file to exist, it is necessary to check that the
        # source file exists and is readable.
        if not os.path.isfile(path):
            logger.error(f"Cannot find file: {path}")
            raise FileNotFoundError(path)
        else:
            try:
                with open(path, "rb"):
                    pass
            except PermissionError:
                logger.error(f"No read permissions: {path}")
                raise

    async def stage(self, transfer_request):
        """
        Stage a transfer request for transfer to an archive

        :param      transfer_request:  The transfer request
        :type       transfer_request:  TransferRequest object
        """
        # tar --dereference -cvf toast.tar toast.txt
        # fetch can stage files using softlinks
        #
        logger.info(f"Staging {transfer_request}")

        # Fetch the description of the files for transfer
        data_descriptor = transfer_request.get_data_descriptor()
        logger.debug(f"Data descriptor {data_descriptor}")

        # Create a temporary base path for all data associated with this
        # transfer request. Base paths are uniquely identified by the
        # transfer request ID. e.g. /base/staging/path/ID/
        base_path = os.path.join(self._staging_path, str(transfer_request.id))
        os.makedirs(base_path, exist_ok=True)
        logger.debug(f"Created staging directory: {base_path}")

        # Sanitise the transfer path to avoid writing to the root partion
        base_transfer_path = data_descriptor["base_transfer_path"].lstrip("/")
        transfer_path = os.path.join(base_path, base_transfer_path)
        os.makedirs(transfer_path, exist_ok=True)
        logger.debug(f"Created transfer directory: {transfer_path}")

        # Iterate over all files to be archived
        for data_product in data_descriptor["data_products"]:
            # fetch the full path of the original data product
            source_path = data_product["source_path"]
            self.verify_file(source_path)

            # fetch the name as it should appear in the archive
            transfer_filename = data_product["transfer_filename"]

            # create the final directory structure for this file
            dest_path = os.path.join(transfer_path, transfer_filename)

            # symlink the source to the new filename
            logger.debug(f"Linking {source_path} --> {dest_path}")
            os.symlink(source_path, dest_path)

        # update the data_descriptor with the used staging path
        # and flag the transfer request as being staged
        data_descriptor["staging_path"] = base_path
        transfer_request.set_data_descriptor(data_descriptor)
        transfer_request.staged = True
        logger.info(f"{transfer_request} successfully staged")

    async def unstage(self, transfer_request):
        """
        Unstage a transfer request

        :param      transfer_request:  The transfer request
        :type       transfer_request:  TransferRequest object

        :detail  As the LocalFileSystemProxy only uses the transfer request ID
                 to uniquely identify staging areas, any corresponding staging
                 area can be deleted without concern for other staged requests.
        """
        logger.info(f"Unstaging {transfer_request}")
        base_path = os.path.join(self._staging_path, str(transfer_request.id))

        # no exception is raised if the base path does not exist
        if os.path.isdir(base_path):
            shutil.rmtree(base_path)
        else:
            logger.warning(
                f"Request to unstage {transfer_request} "
                f"but {base_path} did not exist")
        logger.info(f"{transfer_request} successfully unstaged")







