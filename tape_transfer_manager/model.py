# coding: utf-8
import json
from sqlalchemy import (
    Column, DateTime, ForeignKey, create_engine,
    String, Text, Integer, UniqueConstraint)
from sqlalchemy.orm import relationship, Session
from sqlalchemy.sql.functions import now
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session

Base = declarative_base()
metadata = Base.metadata


class Location(Base):
    __tablename__ = "location"

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False, unique=True)
    description = Column(Text, nullable=True)

    def __repr__(self):
        return f"<Location: {self.name}>"


class StorageDeviceType(Base):
    __tablename__ = "storage_device_type"

    id = Column(Integer, primary_key=True)
    name = Column(String(256), nullable=False, unique=True)
    description = Column(Text, nullable=True)

    devices = relationship(
        "StorageDevice", back_populates="type")

    def __repr__(self):
        return f"<StorageDevice: {self.name}>"


class StorageDeviceState(Base):
    __tablename__ = "storage_device_state"

    id = Column(Integer, primary_key=True)
    name = Column(String(256), nullable=False, unique=True)
    description = Column(Text, nullable=True)

    def __repr__(self):
        return f"<StorageDeviceState: {self.name}>"


class StorageDevice(Base):
    __tablename__ = "storage_device"

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False, unique=True)
    uuid = Column(String(256), nullable=False, unique=True)
    location_id = Column(ForeignKey("location.id"), nullable=False)
    destination_id = Column(ForeignKey("location.id"), nullable=True)
    state_id = Column(ForeignKey("storage_device_state.id"), nullable=False)
    type_id = Column(ForeignKey("storage_device_type.id"), nullable=False)

    type = relationship("StorageDeviceType", back_populates="devices")
    requests = relationship("TransferRequest", back_populates="device")
    location = relationship(
        "Location",
        primaryjoin="Location.id == StorageDevice.location_id")
    destination = relationship(
        "Location",
        primaryjoin="Location.id == StorageDevice.destination_id")

    def __repr__(self):
        return f"<StorageDevice: {self.name}: {self.uuid}>"


class StorageProxy(Base):
    __tablename__ = "storage_proxy"

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False, unique=True)
    description = Column(Text, nullable=True)
    parameters = Column(Text, nullable=False, default="{}")

    def __repr__(self):
        return f"<StorageProxy: {self.name}>"


class TransferRequestState(Base):
    __tablename__ = "transfer_request_state"

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False, unique=True)
    description = Column(Text, nullable=True)

    requests = relationship("TransferRequest", back_populates="state")

    def __repr__(self):
        return f"<TransferRequestState: {self.name}>"


class TransferRequest(Base):
    __tablename__ = "transfer_request"

    id = Column(Integer, primary_key=True)
    project = Column(String(64), nullable=False)
    requested_by = Column(String(64), nullable=False)
    requested_at = Column(DateTime, nullable=False, default=now())
    modified_at = Column(DateTime, nullable=False, default=now())
    priority = Column(Integer, default=0)
    state_id = Column(ForeignKey("transfer_request_state.id"), nullable=False)
    storage_device_id = Column(ForeignKey("storage_device.id"), nullable=True)
    storage_proxy_id = Column(ForeignKey("storage_proxy.id"), nullable=False)
    location_id = Column(ForeignKey("location.id"), nullable=False)
    destination_id = Column(ForeignKey("location.id"), nullable=False)
    data_descriptor = Column(Text, nullable=False)
    size = Column(Integer, nullable=True)
    checksum = Column(Integer, nullable=True)
    staged = Column(Integer, nullable=False, default=0)

    state = relationship("TransferRequestState")
    device = relationship("StorageDevice", back_populates="requests")
    proxy = relationship("StorageProxy")
    location = relationship("Location", primaryjoin="Location.id == TransferRequest.location_id")
    destination = relationship("Location", primaryjoin="Location.id == TransferRequest.destination_id")

    @property
    def name(self):
        return f"TR-{self.project}-{self.id}"

    def __repr__(self):
        return f"<TransferRequest: {self.name}>"


def create_test_database(engine):
    # engine = create_engine("sqlite://", echo=True, future=True)
    metadata.create_all(engine)
    with Session(engine) as s:
        # Create Location
        kdra = Location(
            name="SA::Karoo::MeerKAT::KDRA",
            description="KDRA data centre on the MeerKAT site")
        bonn = Location(
            name="DE::Bonn::MPIfR",
            description="MPIfR in Bonn")
        s.add(kdra)
        s.add(bonn)

        # Create StorageDeviceType
        lto8_tape = StorageDeviceType(
            name="LTO-8",
            description="LTO-8 magnetic tape")
        s.add(lto8_tape)
        s.flush()

        # Create StorageDeviceState
        states = [
            "writable",
            "full",
            "ready_for_transfer",
            "in_transfer",
            "readable",
            "clear",
            "unavailable"
        ]
        for state in states:
            s.add(StorageDeviceState(name=state))
        s.flush()
        writable_state = s.query(StorageDeviceState).filter(
            StorageDeviceState.name == "writable").one()

        # Create StorageDevice
        tape = StorageDevice(
            name="MKT0001", uuid="0x6ef19b02e9ddf64c46d9ce00",
            type_id=lto8_tape.id, location_id=kdra.id,
            destination_id=bonn.id, state_id=writable_state.id)
        s.add(tape)

        # Create StorageProxy
        local_filesystem_proxy = StorageProxy(name="local_filesystem")
        s.add(local_filesystem_proxy)

        # Create TransferRequestState
        state = TransferRequestState(name="pending")
        s.add(state)
        s.flush()

        # Create TransferRequest
        request = TransferRequest(
            project="SCI-20200702-MK-01",
            requested_by="test framework",
            state_id=state.id,
            storage_device_id=tape.id,
            storage_proxy_id=local_filesystem_proxy.id,
            location_id=kdra.id,
            destination_id=bonn.id,
            data_descriptor=json.dumps({"filename": "/tmp/test.txt"}))

        s.add(request)
        s.commit()




