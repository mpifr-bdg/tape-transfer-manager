import logging
import asyncio


class StagingPool:
    """
    This class implementes a pool of asynchronous staging tasks
    for TransferRequest instances.
    """
    def __init__(self, max_size=20):
        """
        Constructs a new instance.

        :param      max_size:  The maximum number of concurrent staging tasks
        :type       max_size:  int
        """
        log.debug((f"Creating {self.__class__.__name__} "
                   f"instance with size: {max_size}"))
        self._max_size = max_size
        self._staging_tasks = set()
        self._lock = asyncio.Lock()

    def task_count(self):
        """
        Return the number of currently staging tasks
        """
        return len(self._staging_tasks)

    def full(self):
        """
        Determines if staging pool is full.
        """
        return self.task_count() < self._max_size

    def empty(self):
        """
        Determines if staging pool is empty
        """
        return self.task_count() == 0

    async def put(self, transfer_request):
        """
        Put a transfer request into the staging pool

        :detail     Transfer requests pushed to the staging pool will
                    have their `fetch` methods called to initiate staging

        :param      transfer_request:  A transfer request to stage
        :type       transfer_request:  TransferRequest instance
        """
        while True:
            if not self.full():
                log.debug(f"Pushing {transfer_request.name} to staging pool")
                staging_task = asyncio.create_task(
                    transfer_request.fetch(),
                    name=f"staging({transfer_request.name})")
                self._staging_tasks.add(staging_task)
                return
            else:
                await asyncio.sleep(0)

    async def get(self):
        """
        Get a staged transfer request from the pool

        :detail    Staged transfer requests are locally
                   avaialble and can be instantly read
        """
        while True:
            for task in self._staging_tasks:
                if task.done():
                    log.debug(f"Staging of {task.name} complete")
                    self._tasks.remove(task)
                    return await task
            else:
                await asyncio.sleep(0)


class TransferManager:
    def __init__(self):
        pass



